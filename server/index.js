const express = require('express');
const BodyParser = require('body-parser');
const path = require('path');

const app = express();
const Router = express.Router();
const staticPath = '../static';
const Port = 8888;

app.use('/', Router);
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());
app.use('/static', express.static(path.join(__dirname, staticPath)));

Router.get('/', (req, res, next) => {
    res.sendFile(path.join(__dirname, staticPath, 'index.html'));
});


app.listen(Port, () => {
    console.log(`\n\nServer listening to port: ${Port}`);
});

module.exports = Router;